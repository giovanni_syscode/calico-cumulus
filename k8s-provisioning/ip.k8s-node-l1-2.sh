#!/bin/sh

nmcli con modify "System eth0" ipv4.address "10.0.101.102/24"
nmcli con modify "System eth0" ipv4.routes "10.0.0.0/8 10.0.101.1"
nmcli con modify "System eth0" ipv4.method manual
nmcli con down "System eth0"
nmcli con up "System eth0"

echo "KUBELET_EXTRA_ARGS='--node-ip 10.0.101.102'" > /etc/sysconfig/kubelet